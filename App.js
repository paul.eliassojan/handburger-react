import { createAppContainer } from "react-navigation";
import { createDrawerNavigator } from 'react-navigation-drawer'; 
import HomeScreen from "./HomeScreen";
import SettingsScreen from "./SettingsScreen";

const AppNavigator = createDrawerNavigator({
  Home: {
    screen: HomeScreen
  },
  Settings: {
    screen: SettingsScreen
  }
});

export default createAppContainer(AppNavigator);